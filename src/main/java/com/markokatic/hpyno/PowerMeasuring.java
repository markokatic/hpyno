
package com.markokatic.hpyno;

import java.util.Date;
import java.util.LinkedList;
import org.jfree.data.category.DefaultCategoryDataset;

public class PowerMeasuring extends Thread{
    public LinkedList<Float> time = new LinkedList<>();
    public LinkedList<Float> speed = new LinkedList<>();
    private LinkedList<Float> calcTime = new LinkedList<>();
    private LinkedList<Float> calcSpeed = new LinkedList<>();
    private LinkedList<Float> power = new LinkedList<>();
    private LinkedList<Float> rpm = new LinkedList<>();
    public float gearRatio = 0;
    int bound1 = 0;
    int bound2 = 0;
    float mass;
    float airRes;
    float surface;
    float airPressure=101325;
    float temperature=0;
    
    
    Window win;
    ELM327Handler elm;
    boolean running = false;
    Date clock = new Date();
    long startedTime;
    
    
    PowerMeasuring(Window w,ELM327Handler e) 
    {
        win = w;
        elm = e;
        startedTime = System.currentTimeMillis();
    }
    
    public DefaultCategoryDataset getGraphData()
    {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        calculatePower(mass,airRes,surface,airPressure,temperature);
        int length = power.size();
        for(int i = 0;i<length;i++)
        {
            dataset.addValue(power.get(i), "Horse Power", rpm.get(i));
        }
        
        return dataset;
    }
    
    private void calculatePower(float mass, float air, float surface, float airPressure, float temperature)
    {
        float airDens = calculateAirDensity(airPressure, temperature);
        setLinkedListsForCalculating();
        float speedLength = calcSpeed.size();
        for(int i = 1;i<speedLength;i++)
        {
            float speed1 = calcSpeed.get(i-1);
            float speed2 = calcSpeed.get(i);
            float avgSpeed = (speed1+speed2)/2;
            float speedDiff =speed2-speed1;
            float timeDiff = (calcTime.get(i)-calcTime.get(i-1))/1000;
            float a = speedDiff/timeDiff;
            float s = speed1*timeDiff + (a*timeDiff*timeDiff)/2;
            float fa = (airDens*avgSpeed*avgSpeed*air*surface)/2;
            System.out.println("Fa: "+fa);
            float f = mass*a + fa;
            float w = f*s;
            float p = (float) ((w/timeDiff)/0.745);
            float r = (float) (avgSpeed*gearRatio*3.6);
            power.addLast(p);
            rpm.addLast(r);
        }
        System.out.println("power size:"+power.size());
        System.out.println("rpm size"+rpm.size());
        System.out.println(rpm);
    }
    
    private float calculateAirDensity(float airPressure, float temperature)
    {
        return (float) (airPressure/(287.05*(temperature+273)));
    }
    
    private void getMeasurementBounds()
    {
       int currentX=1;
       int currentY=0;
       int bestX=1;
       int bestY=0;
       int speedLength = speed.size();
       for(int i = 1;i<speedLength;i++)
       {
           if(speed.get(i)>=speed.get(i-1))
           {
               currentX = i;
               if((currentX-currentY)>(bestX-bestY))
               {
                   bestX = currentX;
                   bestY = currentY;
               }
           }
           else
           {
           currentY = i;
           }
       }
       bound1 = bestY;
       bound2 = bestX;
    }
    
    private void setLinkedListsForCalculating()
    {
        getMeasurementBounds();
        int i;
        int j = 1;
        for(i = bound1; i <= bound2;i++)
        {
            try
            {
            if(speed.get(i)>speed.get(i-j)+5)
            {
                float sp = (float) (speed.get(i)/3.6);
                calcSpeed.addLast(sp);
                calcTime.addLast(time.get(i));
                j = 1;
            }
            else
            {
                j++;
            }
            }
            catch(IndexOutOfBoundsException e)
            {
                //Do nothing
            }
            
        }
        speed.clear();
        time.clear();
    }
   
    public void startThread()
    {
        running = true;
        this.start();
    }
    
    public void stopThread()
    {
        running = false;
    }
    
    @Override
    public void run()
    {
        win.instructions.setText("Getting gear ratio...");
        gearRatio = elm.getGearRatio();

        win.instructions.setText("Application is read to measure.");      
        while(running == true)
        {
            speed.addLast((float) elm.getSpeed());
            time.addLast((float)(System.currentTimeMillis()-startedTime));
        }
        
    }
  
}