package com.markokatic.hpyno;

import java.awt.Color;
import java.awt.Dimension;
import java.util.LinkedList;
import javax.swing.JPanel;
import org.jfree.chart.*;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

public class Graph extends JPanel{
      
    Graph()
    {
        this.setBackground(Color.gray);
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        paintGraph(dataset);
    }
    
    public void paintGraph(DefaultCategoryDataset dataset)
    {
        JFreeChart chart = ChartFactory.createLineChart(
            TOOL_TIP_TEXT_KEY,
            TOOL_TIP_TEXT_KEY,
            TOOL_TIP_TEXT_KEY,
            dataset,
            PlotOrientation.VERTICAL,
            true,
            true, 
            true);
    ChartPanel chartpanel = new ChartPanel(chart);
    chartpanel.setPreferredSize(new Dimension(1000,500));
    this.removeAll();
    this.add(chartpanel,0);
    this.validate();
    }
}
