package com.markokatic.hpyno;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.border.Border;



public class Window extends JFrame implements ActionListener{
    
    JTextField massTextField = new JTextField();
    JTextField airTextField  = new JTextField();
    JTextField surfaceTextField =  new JTextField();
    
    JButton startButton = new JButton();
    JButton stopButton = new JButton();
    
    JLabel massLabel = new JLabel();
    JLabel airLabel = new JLabel();
    JLabel surfaceLabel = new JLabel();
    JLabel instructions = new JLabel();
    
    Date clock = new Date();
    
    ELM327Handler elm;
    
    boolean isRunning = false;

    
    PowerMeasuring powerMeasuring = new PowerMeasuring(this,elm);
    
    Graph graph = new Graph();
    
    Window(int width, int height, String title) throws InterruptedException
    {
        massTextField.setBounds(200, 100, 250, 40);
        airTextField.setBounds(200, 150, 250, 40);
        surfaceTextField.setBounds(200, 200, 250, 40);
        
        startButton.setBounds(350, 270, 100, 35);
        startButton.setText("Start");
        startButton.addActionListener(this);

        stopButton.setBounds(230, 270, 100, 35);
        stopButton.setText("Stop");
        stopButton.addActionListener(this);
        
        massLabel.setText("Total mass of the car:");
        airLabel.setText("Air resistence coefficient:");
        surfaceLabel.setText("Frontal car surface:");
        
        massLabel.setBounds(30,100,150,40);
        airLabel.setBounds(30, 150, 150, 40);
        surfaceLabel.setBounds(30, 200, 150, 40);
        instructions.setBounds(30,300,350,40);
        
        graph.setBounds(500, 100,1010,510);
        this.add(massTextField);
        this.add(airTextField);
        this.add(surfaceTextField);
        this.add(startButton);
        this.add(stopButton);
        this.add(massLabel);
        this.add(airLabel);
        this.add(surfaceLabel);
        this.add(instructions);
        this.add(graph);
        this.setSize(width,height);
        this.setLayout(null);
        this.setTitle(title);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.setExtendedState(this.getExtendedState() | JFrame.MAXIMIZED_BOTH);
        elm = new ELM327Handler();
        elm.setup();
    }
    
    
    private void startButtonPressed()
    {
        powerMeasuring.mass = Float.parseFloat(massTextField.getText());
        powerMeasuring.airRes = Float.parseFloat(airTextField.getText());
        powerMeasuring.surface = Float.parseFloat(surfaceTextField.getText());
        powerMeasuring.startThread();
    }
    
    private void stopButtonPressed ()
    {
        powerMeasuring.stopThread();
        graph.paintGraph(powerMeasuring.getGraphData());
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==startButton)
        {
           startButtonPressed();
        }
        if(e.getSource()==stopButton)
        {
            stopButtonPressed();
        }
    }
}