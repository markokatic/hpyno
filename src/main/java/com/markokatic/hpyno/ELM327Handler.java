package com.markokatic.hpyno;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;
import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ELM327Handler implements SerialPortDataListener{
    
    
    String serialPort = "COM4";
    SerialPort port =  SerialPort.getCommPort(serialPort);
    OutputStream out = this.port.getOutputStream();
    LinkedList<String> listOfCommands = new LinkedList();
    int que = 0;
    String lastMessage = new String();
    
    ELM327Handler()
    {
        this.port.openPort();
        this.port.flushIOBuffers();
        this.port.setBaudRate(9600);
        this.port.setNumDataBits(8);
        this.port.setNumStopBits(1);
        this.port.setParity(0);
        this.port.setComPortTimeouts(300000,300000,300000);
        this.port.addDataListener(this);
    }
    
    
    public String send(String command)
    {   
            this.que = 1;
            byte[] msg = new byte[command.length()];
            msg = command.getBytes();
        try {
            this.out.write(msg);
        } catch (IOException ex) {
            Logger.getLogger(ELM327Handler.class.getName()).log(Level.SEVERE, null, ex);
        }
            int i = 0;
            while(this.que>0)
            {
                try {
                    Thread.sleep((long) 0.01);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ELM327Handler.class.getName()).log(Level.SEVERE, null, ex);
                }
                i++;
            }
            return this.lastMessage;
    }
    
    
    private String read()
    {
        byte[] newData = new byte[port.bytesAvailable()];
        int numRead = port.readBytes(newData, newData.length);
        return new String(newData,0, newData.length);
    }

    public void closePort()
    {
        this.port.closePort();
    }
   
    
    public void setup() 
    {
       String commands[] = {"atz\r","atsp0\r","ate0\r","atl0\r","ath0\r","ats0\r"};
       try
       {
            for(String c : commands)
             {
                this.send(c);
                Thread.sleep(100);
             } 
             Thread.sleep(1000);
             this.send("010c1\r");
             Thread.sleep(5000);
       }catch(InterruptedException e){}
    }
    
    
    private int getIntFromHex(String str)
    {
        String hex = new String();
        
        try
        {
            hex = str.substring(4).replaceAll("\r","").replaceAll(">","");
        }
        catch(StringIndexOutOfBoundsException e)
        {
            return -1;
        }
        
        try
        {
            return Integer.parseInt(hex,16);
        }
        catch(NumberFormatException e)
        {
        return -1;
        }
        
    }
    
    public int getPressure()
    {
       return getIntFromHex(send("01331\r"))*1000;
    }
    
    public int getTemperature()
    {
       return getIntFromHex(send("01461\r"))-40;
    }
    
    public int getSpeed()
    {
        return getIntFromHex(send("010d1\r"));
    }
    
    public int getRpm()
    {
        return (getIntFromHex(send("010C2\r")))/4;
    }
    
    public int getThrottlePosition()
    {
        return getIntFromHex(send("01111\r").replaceAll(" ","").replaceAll("\r",""));
    }
    
    public int getTimingAdvanced()
    {
        return (getIntFromHex(send("010E1\r"))-64)/2;
    }
    
     public int getIntakeAirTemperature()
    {
        return getIntFromHex(send("010F1\r"));
    }
     
     public int getManifoldPressure()
    {
        return getIntFromHex(send("010B1\r"));
    }
     
    public float getGearRatio()
    {
        float speed = 0;
        float speed2 = 0;
        float rpm = 0;
        float gr = 0;
        while(gr == 0)
        {
            while(speed <= 0)
            {
                speed = getSpeed();
                System.out.println("speed1: "+speed);
            }
            while(rpm<=0)
            {
                rpm = getRpm();
                System.out.println("rpm: : "+rpm);
            }
            while(speed2 <= 0)
            {
                speed2 = getSpeed();
                System.out.println("speed2: "+speed2);
            }
            float avgSp = (speed+speed2)/2;
            gr = rpm/avgSp;
            
        }
        return gr;
    }
    
    @Override
    public int getListeningEvents() {
        return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
    }

    @Override
    public void serialEvent(SerialPortEvent event) 
    {
        if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE){return;}
        this.lastMessage = this.read();
        this.que = 0;
    }
}
